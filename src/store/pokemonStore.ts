// import { makeAutoObservable } from 'mobx';
// import { PokemonService } from '@/service/pokemonService';
// import { PokemonFilterService } from '@/service/pokemonFilterService';
// import { Pokemon, PokemonType } from '@/model/pokemon';

// class PokemonStore {
//   pokemons: Pokemon[] = [];
//   filteredPokemons: Pokemon[] = [];
//   types: PokemonType[] = [];
//   isLoading: boolean = false;
//   currentPage: number = 1;
//   selectedType: string = '';

//   pokemonService: PokemonService;
//   pokemonFilterService: PokemonFilterService;

//   constructor() {
//     makeAutoObservable(this);
//     this.pokemonService = new PokemonService();
//     this.pokemonFilterService = new PokemonFilterService();
//   }

//   async loadTypes(): Promise<void> {
//     this.isLoading = true;
//     try {
//       const fetchedTypes = await this.pokemonFilterService.getPokemonTypes();
//       this.types = fetchedTypes;
//     } catch (error) {
//       console.error('Failed to load types', error);
//     } finally {
//       this.isLoading = false;
//     }
//   }

//   async loadPokemons(): Promise<void> {
//     this.isLoading = true;
//     try {
//       const offset =
//         (this.currentPage - 1) * this.pokemonService.getItemsPerPage();
//       const limit = this.pokemonService.getItemsPerPage();
//       const pokemons = await this.pokemonService.getPokemons(offset, limit);
//       pokemons.forEach((pokemon: Pokemon) => {
//         pokemon.isCaught = this.pokemonService.isPokemonCaught(pokemon.id);
//       });
//       this.pokemons = pokemons;
//     } catch (error) {
//       console.error('Failed to load pokemons', error);
//     } finally {
//       this.isLoading = false;
//     }
//   }

//   async loadPokemonsByType(typeName: string): Promise<void> {
//     this.isLoading = true;
//     try {
//       const pokemonsByType = await this.pokemonFilterService.getPokemonsByType(
//         typeName
//       );
//       pokemonsByType.forEach((pokemon: Pokemon) => {
//         pokemon.isCaught = this.pokemonService.isPokemonCaught(pokemon.id);
//       });
//       this.filteredPokemons = pokemonsByType;
//       this.currentPage = 1;
//     } catch (error) {
//       console.error('Failed to load pokemons by type', error);
//     } finally {
//       this.isLoading = false;
//     }
//   }

//   setCurrentPage(page: number): void {
//     this.currentPage = page;
//   }

//   setSelectedType(type: string): void {
//     this.selectedType = type;
//   }

//   paginatePokemons(): void {
//     const offset =
//       (this.currentPage - 1) * this.pokemonService.getItemsPerPage();
//     const limit = this.pokemonService.getItemsPerPage();
//     if (this.selectedType) {
//       this.pokemons = this.filteredPokemons.slice(offset, offset + limit);
//     } else {
//       this.loadPokemons();
//     }
//   }

//   async catchPokemon(pokemon: Pokemon): Promise<void> {
//     await this.pokemonService.catchPokemon(pokemon);
//     if (this.selectedType) {
//       this.loadPokemonsByType(this.selectedType);
//     } else {
//       this.loadPokemons();
//     }
//   }
// }

// export const pokemonStore = new PokemonStore();
