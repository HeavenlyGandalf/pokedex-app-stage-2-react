import { Pokemon } from '@/model/pokemon';
import { IPokemonStore } from '@/model/pokemonStore';
import { makeAutoObservable } from 'mobx';
export class CaughtPokemonStore implements IPokemonStore {
  private static instance: CaughtPokemonStore;
  caughtPokemons: Pokemon[] = [];

  private constructor() {
    makeAutoObservable(this);
    this.loadCaughtPokemonsFromLocalStorage();
  }

  private saveCaughtPokemonsToLocalStorage() {
    localStorage.setItem('caughtPokemons', JSON.stringify(this.caughtPokemons));
  }

  private loadCaughtPokemonsFromLocalStorage() {
    const caughtPokemons = JSON.parse(
      localStorage.getItem('caughtPokemons') || '[]'
    );
    this.caughtPokemons = caughtPokemons;
  }

  public static getInstance(): CaughtPokemonStore {
    if (!CaughtPokemonStore.instance) {
      CaughtPokemonStore.instance = new CaughtPokemonStore();
    }
    return CaughtPokemonStore.instance;
  }

  public catchPokemon(pokemon: Pokemon): void {
    const caughtDate = new Date()
      .toLocaleString('ru-RU', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit',
        hour: '2-digit',
        minute: '2-digit',
        second: '2-digit',
        hour12: false,
      })
      .replace(',', '');

    const caughtPokemon = {
      ...pokemon,
      catchDate: caughtDate,
      isCaught: true,
    };
    this.caughtPokemons = [...this.caughtPokemons, caughtPokemon];
    this.saveCaughtPokemonsToLocalStorage();
  }

  // public getCaughtPokemons(): Pokemon[] {
  //   return this.caughtPokemons;
  // }

  public isPokemonCaught(id: number): boolean {
    return this.caughtPokemons.some((pokemon) => pokemon.id === id);
  }

  public getTotalCaughtPokemonsCount(): number {
    return this.caughtPokemons.length;
  }

  public getPokemonById(id: number): Pokemon | undefined {
    return this.caughtPokemons.find((pokemon) => pokemon.id === id);
  }

  public getCatchDate(id: number): string | undefined {
    const caughtPokemon = this.caughtPokemons.find(
      (pokemon) => pokemon.id === id
    );
    return caughtPokemon ? caughtPokemon.catchDate : undefined;
  }
}

export default CaughtPokemonStore.getInstance();
