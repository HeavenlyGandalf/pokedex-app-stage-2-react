export interface IPaginationService {
  getCurrentPage(): number;
  getTotalItems(): number;
  getItemsPerPage(): number;
  setTotalItems(totalItems: number): void;
  nextPage(): void;
  prevPage(): void;
  getTotalPages(): number;
}
