import { Pokemon } from './pokemon';

export interface PokemonCardProps {
  pokemon: Pokemon;
  onCatch: () => void;
}
