export interface IPokemonApi {
  fetchPokemons(offset: number, limit: number): Promise<any>;
  fetchPokemonDetails(url: string): Promise<any>;
  fetchPokemonDetailsByIdOrName(idOrName: string): Promise<any>;
}
