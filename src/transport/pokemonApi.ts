import { IPokemonApi } from '@/model/pokemonApi';

export class PokemonApi implements IPokemonApi {
  private baseUrl: string;

  constructor() {
    this.baseUrl = 'https://pokeapi.co/api/v2/pokemon';
  }

  public async fetchPokemons(offset: number, limit: number): Promise<any> {
    const response = await fetch(
      `${this.baseUrl}?offset=${offset}&limit=${limit}`
    );
    return await response.json();
  }

  public async fetchPokemonDetails(url: string): Promise<any> {
    const response = await fetch(url);
    return await response.json();
  }

  public async fetchPokemonDetailsByIdOrName(idOrName: string): Promise<any> {
    const response = await fetch(`${this.baseUrl}/${idOrName}`);
    return await response.json();
  }

  public async fetchPokemonTypes() {
    const response = await fetch('https://pokeapi.co/api/v2/type/');
    return await response.json();
  }

  public async fetchPokemonsByType(typeName: string) {
    const response = await fetch(`https://pokeapi.co/api/v2/type/${typeName}`);
    return await response.json();
  }
}
