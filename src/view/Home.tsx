import React, { useEffect, useRef, useState } from 'react';
import { PokemonService } from '@/service/pokemonService';
import { PaginationService } from '@/service/paginationService';
import styles from './styles/styles.module.scss';
import PokemonCard from './PokemonCard/PokemonCard';
import { Pokemon } from '@/model/pokemon';
import { Loader } from './Loader/Loader';
import { PokemonFilterService } from '@/service/pokemonFilterService';
import { observer } from 'mobx-react-lite';
import NoPokemonAvailable from './NoPokemonAvailable/NoPokemonAvailable';

const Home: React.FC = () => {
  const [pokemons, setPokemons] = useState<Pokemon[]>([]);
  const [filteredPokemons, setFilteredPokemons] = useState<Pokemon[]>([]);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [selectedType, setSelectedType] = useState<string>('');
  const [types, setTypes] = useState<{ name: string }[]>([]);

  const pokemonFilterService = new PokemonFilterService();
  const paginationService = useRef(new PaginationService()).current;
  const pokemonService = useRef(new PokemonService()).current;

  useEffect(() => {
    loadTypes();
    loadPokemons();
  }, []);

  useEffect(() => {
    if (selectedType) {
      loadPokemonsByType(selectedType);
    } else {
      loadPokemons();
    }
  }, [selectedType]);

  useEffect(() => {
    paginatePokemons();
  }, [currentPage, filteredPokemons]);

  const loadTypes = async () => {
    try {
      const fetchedTypes = await pokemonFilterService.getPokemonTypes();
      setTypes(fetchedTypes);
    } catch (error) {
      console.error('Failed to load types', error);
    }
  };
  const handleResetFilter = () => {
    setSelectedType('');
  };

  const loadPokemons = async () => {
    setIsLoading(true);
    try {
      const offset = (currentPage - 1) * paginationService.getItemsPerPage();
      const limit = paginationService.getItemsPerPage();

      const pokemons = await pokemonService.getPokemons(offset, limit);
      const totalPokemons = await pokemonService.getTotalPokemonsCount();

      paginationService.setTotalItems(totalPokemons);

      pokemons.forEach((pokemon: Pokemon) => {
        pokemon.isCaught = pokemonService.isPokemonCaught(pokemon.id);
      });

      setPokemons(pokemons);
    } catch (error) {
      console.error('Failed to load pokemons', error);
    } finally {
      setIsLoading(false);
    }
  };

  const loadPokemonsByType = async (typeName: string) => {
    setIsLoading(true);
    try {
      const pokemonsByType = await pokemonFilterService.getPokemonsByType(
        typeName
      );
      pokemonsByType.forEach((pokemon: Pokemon) => {
        pokemon.isCaught = pokemonService.isPokemonCaught(pokemon.id);
      });

      setFilteredPokemons(pokemonsByType);
      paginationService.setTotalItems(pokemonsByType.length);
      setCurrentPage(1);
    } catch (error) {
      console.error('Failed to load pokemons by type', error);
    } finally {
      setIsLoading(false);
    }
  };

  const paginatePokemons = () => {
    const offset = (currentPage - 1) * paginationService.getItemsPerPage();
    const limit = paginationService.getItemsPerPage();
    if (selectedType) {
      setPokemons(filteredPokemons.slice(offset, offset + limit));
    } else {
      loadPokemons();
    }
  };

  const handleTypeChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setSelectedType(event.target.value);
  };

  const catchPokemon = async (pokemon: Pokemon) => {
    await pokemonService.catchPokemon(pokemon);

    if (selectedType) {
      loadPokemonsByType(selectedType);
    } else {
      loadPokemons();
    }
  };

  const handlePrevPage = () => {
    if (currentPage > 1) {
      paginationService.prevPage();
      setCurrentPage(paginationService.getCurrentPage());
    }
  };

  const handleNextPage = () => {
    if (currentPage < paginationService.getTotalPages()) {
      paginationService.nextPage();
      setCurrentPage(currentPage + 1);
    }
  };

  return (
    <div>
      {isLoading ? (
        <Loader />
      ) : (
        <>
          <div className={styles.filterContainer}>
            <select
              className={styles.select}
              onChange={handleTypeChange}
              value={selectedType}
            >
              <option value=''>Выберите тип</option>
              {types.map((type) => (
                <option key={type.name} value={type.name}>
                  {type.name}
                </option>
              ))}
            </select>
            <button className={styles.resetButton} onClick={handleResetFilter}>
              Сбросить фильтр
            </button>
          </div>

          {pokemons.length === 0 ? (
            <NoPokemonAvailable />
          ) : (
            <section>
              <div className={styles.pokemons}>
                {pokemons.map((pokemon) => (
                  <PokemonCard
                    key={pokemon.id}
                    pokemon={pokemon}
                    onCatch={() => catchPokemon(pokemon)}
                  />
                ))}
              </div>

              <div id='pagination' className={styles.pagination}>
                <button
                  id='prev'
                  onClick={handlePrevPage}
                  disabled={currentPage === 1}
                >
                  &lt;
                </button>
                <span id='page-info'>{currentPage}</span>
                <button
                  id='next'
                  onClick={handleNextPage}
                  disabled={currentPage === paginationService.getTotalPages()}
                >
                  &gt;
                </button>
              </div>
            </section>
          )}
        </>
      )}
    </div>
  );
};

export default observer(Home);
