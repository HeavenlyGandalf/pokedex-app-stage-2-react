import React from 'react';
import styles from './NoPokemonAvailable.module.scss';

const NoPokemonAvailable: React.FC = () => {
  return (
    <div className={styles.container}>
      <img
        src='https://pngimg.com/uploads/pokemon/pokemon_PNG98.png'
        alt='No Pokemon Found'
        className={styles.image}
      />
      <h2 className={styles.title}>No Pokemon Found</h2>
      <p className={styles.message}>
        Unfortunately, no Pokemon were found. Try changing the filters or
        refreshing the page.
      </p>
    </div>
  );
};

export default NoPokemonAvailable;
