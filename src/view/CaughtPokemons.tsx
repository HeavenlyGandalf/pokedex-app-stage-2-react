import React, { useState, useEffect } from 'react';
import { Pokemon } from '@/model/pokemon';
import styles from './styles/styles.module.scss';
import PokemonCard from './PokemonCard/PokemonCard';
import { NoPokemonPage } from './NoPokemon/NoPokemonPage';
import { observer } from 'mobx-react-lite';
import pokemonStore from '@/store/CaughtPokemonStore';
import { PaginationService } from '@/service/paginationService';

const CaughtPokemons: React.FC = () => {
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [paginatedPokemons, setPaginatedPokemons] = useState<Pokemon[]>([]);
  const paginationService = new PaginationService();

  useEffect(() => {
    paginationService.setTotalItems(pokemonStore.caughtPokemons.length);
    paginatePokemons();
  }, [currentPage, pokemonStore.caughtPokemons]);

  const paginatePokemons = () => {
    const offset = (currentPage - 1) * paginationService.getItemsPerPage();
    const limit = paginationService.getItemsPerPage();
    const paginated = pokemonStore.caughtPokemons.slice(offset, offset + limit);
    setPaginatedPokemons(paginated);
  };

  const handlePrevPage = () => {
    paginationService.prevPage();
    setCurrentPage(paginationService.getCurrentPage());
  };

  const handleNextPage = () => {
    paginationService.nextPage();
    setCurrentPage(paginationService.getCurrentPage());
  };

  return (
    <>
      <div className={styles.pokemons}>
        {paginatedPokemons.length === 0 ? (
          <NoPokemonPage />
        ) : (
          paginatedPokemons.map((pokemon) => (
            <PokemonCard
              key={pokemon.id}
              pokemon={pokemon}
              onCatch={() => {}}
            />
          ))
        )}
      </div>

      {paginatedPokemons.length > 0 &&
        paginationService.getTotalItems() >
          paginationService.getItemsPerPage() && (
          <div id='pagination' className={styles.pagination}>
            <button
              id='prev'
              onClick={handlePrevPage}
              disabled={currentPage === 1}
            >
              &lt;
            </button>
            <span id='page-info'>{currentPage}</span>
            <button
              id='next'
              onClick={handleNextPage}
              disabled={currentPage === paginationService.getTotalPages()}
            >
              &gt;
            </button>
          </div>
        )}
    </>
  );
};

export default observer(CaughtPokemons);
