import React from 'react';
import { useNavigate } from 'react-router-dom';
import styles from './NotFound.module.scss';

const NotFound: React.FC = () => {
  const navigate = useNavigate();

  const handleBackClick = () => {
    navigate('/');
  };

  return (
    <div className={styles.notFound}>
      <h1>404 - Page not found</h1>
      <img
        className={styles.image}
        src='https://i.pinimg.com/originals/60/8e/30/608e3068f873c58776f5f5fce86d9c70.png'
      ></img>
      <p>The page you are looking for does not exist.</p>
      <button className={styles.backButton} onClick={handleBackClick}>
        Go back to the main page
      </button>
    </div>
  );
};

export default NotFound;
