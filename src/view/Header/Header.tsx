import React, { useEffect, useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import styles from './header.module.scss';
import pokemonStore from '@/store/CaughtPokemonStore';
import { observer } from 'mobx-react-lite';

const Header: React.FC = () => {
  const location = useLocation();
  const [activeTab, setActiveTab] = useState<string>('/');

  useEffect(() => {
    setActiveTab(location.pathname);
  }, [location.pathname]);

  return (
    <header>
      <div className={styles.tabs}>
        <Link
          to='/'
          className={`${styles.tab} ${activeTab === '/' ? styles.active : ''}`}
        >
          Главная
        </Link>
        <Link
          to='/caught'
          className={`${styles.tab} ${
            activeTab === '/caught' ? styles.active : ''
          }`}
        >
          Пойманные покемоны
        </Link>
      </div>
      <span className={styles.caughtCount}>
        {pokemonStore.caughtPokemons.length}
      </span>
    </header>
  );
};

export default observer(Header);
