import React from 'react';
import styles from './pokemonCard.module.scss';
import { useNavigate } from 'react-router-dom';
import { PokemonCardProps } from '@/model/props';

const PokemonCard: React.FC<PokemonCardProps> = ({ pokemon, onCatch }) => {
  const navigate = useNavigate();

  const handleCardClick = () => {
    navigate(`/pokemon/${pokemon.id}`);
  };

  const handleCatchClick = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    event.stopPropagation();
    onCatch();
  };

  return (
    <div className={styles.pokemonCard} onClick={handleCardClick}>
      <img src={pokemon.imageUrl} alt={pokemon.name} />
      <div className={styles.detailsContainer}>
        <div className={styles.pokemonInfo}>
          <p>{`#${pokemon.id}`}</p>
          <p>{pokemon.name}</p>
        </div>
        <button
          className={styles.catchButton}
          disabled={pokemon.isCaught}
          onClick={handleCatchClick}
        >
          {pokemon.isCaught ? 'Пойман' : 'Поймать!'}
        </button>
      </div>
    </div>
  );
};

export default PokemonCard;
