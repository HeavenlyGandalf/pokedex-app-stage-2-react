import { Pokemon } from '@/model/pokemon';
import { PokemonApi } from '../transport/pokemonApi';
import { CaughtPokemonStore } from '@/store/CaughtPokemonStore';

export class PokemonFilterService {
  private pokemonStore: CaughtPokemonStore;
  private pokemonApi: PokemonApi;

  constructor() {
    this.pokemonStore = CaughtPokemonStore.getInstance();
    this.pokemonApi = new PokemonApi();
  }

  public async getPokemonsByType(typeName: string): Promise<Pokemon[]> {
    const pokemonsByType = await this.pokemonApi.fetchPokemonsByType(typeName);

    const pokemonPromises = pokemonsByType.pokemon.map(
      async (pokemonEntry: { pokemon: { url: string } }) => {
        const details = await this.pokemonApi.fetchPokemonDetails(
          pokemonEntry.pokemon.url
        );
        return {
          id: details.id,
          name: details.name,
          imageUrl: details.sprites.front_default,
          isCaught: this.pokemonStore.isPokemonCaught(details.id),
          types: details.types.map((type: any) => type.type.name),
        };
      }
    );

    const pokemons = await Promise.all(pokemonPromises);
    return pokemons;
  }

  public async getPokemonTypes(): Promise<{ name: string }[]> {
    const types = await this.pokemonApi.fetchPokemonTypes();
    return types.results.map((type: { name: string; url: string }) => ({
      name: type.name,
    }));
  }
}
