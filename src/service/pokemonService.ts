import { Pokemon, PokemonSummary } from '@/model/pokemon';
import { PokemonApi } from '../transport/pokemonApi';
import { CaughtPokemonStore } from '@/store/CaughtPokemonStore';
import { IPokemonService } from '@/model/pokemonService';

export class PokemonService implements IPokemonService {
  private totalPokemonsCount: number = 0;
  private pokemonStore: CaughtPokemonStore;
  private pokemonApi: PokemonApi;

  constructor() {
    this.pokemonStore = CaughtPokemonStore.getInstance();
    this.pokemonApi = new PokemonApi();
  }

  public async getPokemons(offset: number, limit: number) {
    const data = await this.pokemonApi.fetchPokemons(offset, limit);
    this.totalPokemonsCount = data.count;

    const pokemonPromises = data.results.map(
      async (pokemonSummary: PokemonSummary) => {
        const details = await this.pokemonApi.fetchPokemonDetails(
          pokemonSummary.url
        );
        return {
          id: details.id,
          name: details.name,
          imageUrl: details.sprites.front_default,
          isCaught: this.pokemonStore.isPokemonCaught(details.id),
        };
      }
    );

    const pokemons = await Promise.all(pokemonPromises);
    return pokemons;
  }

  public getTotalPokemonsCount(): number {
    return this.totalPokemonsCount;
  }

  public async catchPokemon(pokemon: Pokemon): Promise<void> {
    this.pokemonStore.catchPokemon(pokemon);
  }

  public getTotalCaughtPokemonsCount(): number {
    return this.pokemonStore.getTotalCaughtPokemonsCount();
  }

  public isPokemonCaught(id: number): boolean {
    return this.pokemonStore.isPokemonCaught(id);
  }

  public getCatchDate(id: number): string | undefined {
    return this.pokemonStore.getCatchDate(id);
  }

  public async getPokemonByIdOrName(idOrName: string): Promise<Pokemon> {
    const details = await this.pokemonApi.fetchPokemonDetailsByIdOrName(
      idOrName
    );
    return {
      id: details.id,
      name: details.name,
      imageUrl: details.sprites.front_default,
      isCaught: this.pokemonStore.isPokemonCaught(details.id),
      abilities: details.abilities.map(
        (abilityData: { ability: { name: string }; slot: number }) => ({
          name: abilityData.ability.name,
          slot: abilityData.slot,
        })
      ),
      types: details.types.map((type: any) => type.type.name),
    };
  }

  public filterPokemonsByType(type: string, pokemons: Pokemon[]): Pokemon[] {
    return pokemons.filter((pokemon) => pokemon.types.includes(type));
  }
}
